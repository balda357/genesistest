﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }


    #region Singleton
    private static AudioController _instance;

    public static AudioController Instance
    {
        get
        {
            if (_instance == null) _instance = FindObjectOfType<AudioController>();

            return _instance;
        }
    }
    #endregion


    [SerializeField] private AudioSource AudioSourceVFX;
    [SerializeField] private AudioSource AudioSourceMusic;


    public void MuteVFX(bool value)
    {
        AudioSourceVFX.mute = value;
    }

    public void MuteMusic(bool value)
    {
        AudioSourceMusic.mute = value;
    }
}
