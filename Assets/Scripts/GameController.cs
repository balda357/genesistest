﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    private UIController uiController;

    private void Start()
    {
        uiController = FindObjectOfType<UIController>();
    }

    #region Singleton
    private static GameController _instance;

    public static GameController Instance
    {
        get
        {
            if (_instance == null) _instance = FindObjectOfType<GameController>();

            return _instance;
        }
    }
    #endregion

    private int coins = 0;
    

    public int Coins
    {
        get
        {
            return coins;
        }
        set
        {
            coins = value;
            uiController.coins.text = coins.ToString();
        }
    }


}
