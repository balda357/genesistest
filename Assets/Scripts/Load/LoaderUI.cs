﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LoaderUI : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI Text;
    [SerializeField] private GameObject noInternetGO;


    private void Start()
    {
        Loader.Instance.ProgressChanged += ShowProgress;
        Loader.Instance.InternetConnection += ShowInternetConnectionWindow;
    }

    private void ShowProgress(string progress)
    {
        if (Text != null)
            Text.text = " Loading... (" + progress + ")";
    }

    private void ShowInternetConnectionWindow(bool isConnected)
    {
        if (noInternetGO != null)
            noInternetGO.SetActive(!isConnected);
    }

}
