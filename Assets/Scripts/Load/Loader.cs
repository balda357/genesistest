﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Loader : MonoBehaviour
{
    [SerializeField] private Image Fader;

    public Action<string> ProgressChanged;

    public Action<bool> InternetConnection;

    public int version = 0;


#if UNITY_STANDALONE
    public string url = "https://drive.google.com/uc?export=download&id=15ck_yXvGjL8ntShEElvmGZh38NGtho1f";
#elif UNITY_ANDROID
    public string url = "https://drive.google.com/uc?export=download&id=1vxasgSHOPRxZ5oc3NBw2w8t3E4XV6tFF";
#endif

    public string AssetName = "gamescene";

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

#region Singleton
    private static Loader _instance;

    public static Loader Instance
    {
        get
        {
            if (_instance == null) _instance = FindObjectOfType<Loader>();

            return _instance;
        }
    }
#endregion


    public void LoadScene(string sceneName)
    {
        StartCoroutine(UnloadSceneCoroutine(SceneManager.GetActiveScene(), true));
        SceneManager.LoadSceneAsync("Loader", LoadSceneMode.Additive);
        StartCoroutine(LoadSceneCoroutine(sceneName));

    }

    private IEnumerator UnloadSceneCoroutine(Scene scene, bool fadeOn)
    {
        for (float i = 0; i <= 1; i += .1f)
        {
            yield return new WaitForSeconds(.1f);
            Fader.color = new Color(Fader.color.r, Fader.color.g, Fader.color.b, fadeOn ? i: 1 - i);
        }
        SceneManager.UnloadSceneAsync(scene);
    }

    private IEnumerator LoadSceneCoroutine(string sceneName)
    {
        SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);

        if (sceneName == "Game")
        {
            yield return new WaitForSeconds(1);
            yield return StartCoroutine(LoadAssetBundle());
        }

        StartCoroutine(UnloadSceneCoroutine(SceneManager.GetSceneByName("Loader"), false));
    }

    private IEnumerator LoadAssetBundle()
    {

        while (!Caching.ready)
            yield return null;

        WWW www = WWW.LoadFromCacheOrDownload(url, version);

        

        while (!www.isDone)
        {

            yield return new WaitForSeconds(.1f); //Каждых .1f секунды пробует переподключиться
            ProgressChanged(www.progress * 100 + "%");

            if (www.error == null)
                InternetConnection(true);
            else
                InternetConnection(false);
        }

        if (www.error == null)
        {
            AssetBundle bundle = www.assetBundle;

            SceneManager.SetActiveScene(SceneManager.GetSceneByName("Game"));

            // Здесь можно было бы плучить список и загрузить в цикле. 

            GameObject gameController = bundle.LoadAsset("GameController") as GameObject;
            Instantiate(gameController);

            //GameObject directionLight = bundle.LoadAsset("Directional Light") as GameObject;
            //Instantiate(directionLight);

            GameObject canvas = bundle.LoadAsset("Canvas") as GameObject;
            Instantiate(canvas);

            GameObject eventSystem = bundle.LoadAsset("EventSystem") as GameObject;
            Instantiate(eventSystem);

            GameObject plane = bundle.LoadAsset("Plane") as GameObject;
            Instantiate(plane);

            GameObject character = bundle.LoadAsset("Character") as GameObject;
            Instantiate(character);

            GameObject coinsSpawner = bundle.LoadAsset("CoinsSpawner") as GameObject;
            Instantiate(coinsSpawner);

            bundle.Unload(false);
        }
    }






    private void OnApplicationQuit()
    {
        Application.Quit();
    }
}
