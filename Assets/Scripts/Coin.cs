﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    private bool isGrounded = false;

    // Update is called once per frame
    void Update()
    {
        if (isGrounded)
        {
            transform.Rotate(Vector3.up, 50 * Time.deltaTime);
        }
        else
        {
            int g = 10;

            transform.Translate(Vector3.down * Time.deltaTime * g);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ground")
        {
            isGrounded = true;
            Destroy(gameObject, 5);

        }

        if (other.tag == "Player")
        {
            GameController.Instance.Coins++;
            Destroy(gameObject);

        }
    }

}
