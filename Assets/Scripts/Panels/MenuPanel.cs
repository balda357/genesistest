﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuPanel : Panel
{
    
    public void OnPlayButton()
    {
        Loader.Instance.LoadScene("Game");
    }

    public void OnSettingsButton()
    {
        PanelManager.Instance.OpenPanel<SettingsPanel>();
    }
}
