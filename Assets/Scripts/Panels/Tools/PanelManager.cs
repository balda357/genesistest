﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PanelManager : MonoBehaviour
{
#pragma warning disable CS0649
    [SerializeField] private Transform panelsRoot;
    [SerializeField] private List<Panel> panels;


    private List<Panel> openedPanels;

    private Panel CurrentPanel;


    #region Singleton
    private static PanelManager _instance;

    public static PanelManager Instance
    {
        get
        {
            if (_instance == null) _instance = FindObjectOfType<PanelManager>();

            return _instance;
        }
    }
    #endregion


    public void OpenPanel<T>() where T : Panel
    {
        if (CurrentPanel != null)
            AnimatePanelClose(CurrentPanel.transform);

        if (openedPanels == null) openedPanels = new List<Panel>();
        else
        {
            foreach (var p in openedPanels)
            {
                if (p is T)
                {
                    AnimatePanelOpen(p.transform);
                    p.Init();
                    CurrentPanel = p;
                    return;
                }
            }
        }

        foreach (var p in panels)
        {
            if (p is T)
            {
                var panel = Instantiate(p, panelsRoot).GetComponent<T>();
                
                openedPanels.Add(panel);
                AnimatePanelOpen(panel.transform);

                panel.Init();
                CurrentPanel = panel;
            }
        }
    }

    private void AnimatePanelOpen(Transform t)
    {
        t.position = new Vector3(Screen.width + 1000, t.position.y, 0);
        t.DOMoveX(Screen.width/2, 1);
        t.DORestart();
        t.gameObject.SetActive(true);
    }

    private void AnimatePanelClose(Transform t)
    {
        t.DOMoveX(-1000, 1);
        t.DORestart();
    }
}