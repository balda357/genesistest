﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsPanel : Panel
{
    [SerializeField] private Toggle VFXCheckbox;
    [SerializeField] private Toggle MusicCheckbox;

    public void OnBackButton()
    {
        PanelManager.Instance.OpenPanel<MenuPanel>();
    }

    public void OnCheckboxVFX()
    {
        AudioController.Instance.MuteVFX(!VFXCheckbox.isOn);
    }

    public void OnCheckboxMusic()
    {
        AudioController.Instance.MuteMusic(!MusicCheckbox.isOn);
    }

}
