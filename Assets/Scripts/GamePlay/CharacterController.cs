﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    [SerializeField][Range (1, 30)] private int moveSpeed;
    [SerializeField] [Range(50, 300)] private int rotateSpeed;

    [SerializeField] private Transform modelTransform;

    [SerializeField] private Animator animator;

    private new Rigidbody rigidbody;
    private Joystick joystick;


    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        joystick = FindObjectOfType<Joystick>();
    }

    private void Update()
    {
        rigidbody.velocity = new Vector3(joystick.Horizontal * moveSpeed,
                                rigidbody.velocity.y,
                                joystick.Vertical * moveSpeed);

        var input = new Vector3(joystick.Horizontal, 0, joystick.Vertical);
        if (input != Vector3.zero)
        {
            var targetRotation = Quaternion.LookRotation(input);
            modelTransform.rotation = Quaternion.RotateTowards(modelTransform.rotation, targetRotation, rotateSpeed * Time.deltaTime);

            animator.SetBool("Run", true);
        }
        else
        {
            animator.SetBool("Run", false);
        }
    }
}
