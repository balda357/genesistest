﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsSpawner : MonoBehaviour
{
    [SerializeField] private GameObject CoinPrefab;


    private void Start()
    {
        StartCoroutine(Spawn());
    }


    private IEnumerator Spawn()
    {
        while (true)
        {
            yield return new WaitForSeconds(.3f);

            Instantiate(CoinPrefab, new Vector3(Random.Range(-50, 50), 10, Random.Range(-50, 50)), Quaternion.identity, transform);
        }
    }

}
