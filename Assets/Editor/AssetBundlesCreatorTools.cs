﻿using UnityEditor;
using System.IO;
using UnityEngine;

public class AssetBundlesCreatorTools
{
    [MenuItem("Tools/Assets/Build AssetBundles")]
    public static void BuildAllAssetBundles()
    {
        string assetBundleDirectory = "Assets/AssetBundles/Android";
        if (!Directory.Exists(assetBundleDirectory))
        {
            Directory.CreateDirectory(assetBundleDirectory);
        }
        BuildPipeline.BuildAssetBundles(assetBundleDirectory,
                                        BuildAssetBundleOptions.ChunkBasedCompression,
                                        EditorUserBuildSettings.activeBuildTarget);

        Debug.Log("Target: " + EditorUserBuildSettings.activeBuildTarget);
    }

    [MenuItem("Tools/Assets/Clean cache")]
    public static void CleanCache()
    {
        if (Caching.ClearCache())
        {
            Debug.Log("Successfully cleaned the cache.");
        }
        else
        {
            Debug.Log("Cache is being used.");
        }
    }
}
